import Vue from 'vue'
import Framework7 from 'framework7'
import Framework7Vue from 'framework7-vue'
import VueFire from 'vuefire'
import VeeValidate from 'vee-validate'

import routes from './routes'

import './index.scss'
import './index.less'

import './less/index.less'

import App from './App.vue'

Vue.use(Framework7Vue)
Vue.use(VueFire)
Vue.use(VeeValidate)

new Vue({
  el: '#app',
  framework7: {
    root: '#app',
    pushState: true,
    pushStateSeparator: '',
    pushStateNoAnimation: true,
    pushStateRoot: process.env.PUSH_STATE_ROOT,
    routes
  },
  render: h => h(App)
})