import Profile from './pages/Profile'
import Submitted from './pages/Submitted'

export default [
  {
    path: '/',
    component: Profile
  },
  {
    path: '/submitted',
    component: Submitted
  }
]